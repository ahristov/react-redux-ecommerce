# Building React Redux E-commerce

Contains notes and code from studying [Building production e-commerce with React and Redux](https://www.udemy.com/building-a-production-e-commerce-with-react-redux)

Framework:

- React
- React Router
- Redux
- Ramda
- Superagent

To run the app:

```bash
cd code
yarn
yarn start
```